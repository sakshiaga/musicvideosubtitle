from . import views
from django.urls import path

from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('upload_video/', views.upload_video, name='upload_video'),
    path('upload_video/success/', views.success, name='success'),
    # path('video_detail/<int:pk>/', views.video_detail, name='video_detail'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

