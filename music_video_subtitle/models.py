from django.db import models


class MusicVideo(models.Model):
    title = models.CharField(max_length=100)
    video_file = models.FileField(upload_to='media/')
    
    
    def __str__(self):
        return self.video_file.name




class Video(models.Model):
    video_file = models.FileField(upload_to='media/')
    subtitle_file = models.FileField(upload_to='media/', blank=True, null=True)
    timestamped_video_file = models.FileField(upload_to='media/timestamped_videos/', blank=True, null=True)
  

