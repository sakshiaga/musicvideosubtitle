from django import forms
from .models import Video
from .models import MusicVideo

class VideoForm(forms.ModelForm):
    class Meta:
        model = MusicVideo
        fields = ('title','video_file')

