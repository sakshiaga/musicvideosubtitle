from http.client import HTTPResponse
from django.shortcuts import redirect, render
from .forms import VideoForm
from .models import MusicVideo
from .models import Video

import pysrt
import ffmpeg

def upload_video(request):
    if request.method == 'POST':
        form = VideoForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('/upload_video/success/')
    else:
        form = VideoForm() 
    context = {'form': form}
    return render(request, 'upload_video.html', context)

            # subtitle_file = form.cleaned_data['subtitles']
            # timestamped_video_file = convert_video_with_subtitles(video.video_file, subtitle_file)
            # video.timestamped_video_file = timestamped_video_file
            # video.save()

def success(request):
    return render(request, 'success.html')      

# def video_detail(request, pk):
#     video = Video.objects.get(pk=pk)
#     return render(request, 'video_details.html', {'video': video})

# def extract_subtitles(video_file):
#     subtitle_file_path = f'media/subtitles/{video_file.name}.srt'
#     subtitle_file = pysrt.SubRipFile()
#     subtitle_file.save(subtitle_file_path)
#     return subtitle_file_path


# def convert_video_with_subtitles(video_file, subtitle_file):
#     # Extract subtitles from the video
#     extracted_subtitle_file = extract_subtitles(video_file)
#     # Define the output video file path
#     timestamped_video_file_path = f'media/timestamped_videos/{video_file.name}'
#     # Define the output subtitle file path
#     timestamped_subtitle_file_path = f'media/timestamped_subtitles/{video_file.name}.srt'
#     # Perform the conversion using ffmpeg
#     # Replace this code with the appropriate ffmpeg command for your desired conversion
#     # The command should overlay the subtitles on the video using the extracted_subtitle_file
#     # Make sure to adjust the output paths and options according to your needs
#     converted_video_path = f'media/timestamped_videos/{video_file.name}'
#     ffmpeg.input(video_file.path).output(converted_video_path, vf=f"subtitles='{extracted_subtitle_file}':force_style='Alignment=6,FontSize=24'").run()
#     return converted_video_path