from django.contrib import admin
from .models import MusicVideo, Video
# Register your models here.

class MusicVideoAdmin(admin.ModelAdmin):
    list_display=('title','video_file')


admin.site.register(MusicVideo)
    
class VideoAdmin(admin.ModelAdmin):
    list_display=('video_file','subtitle_file','timestamped_video_file')
    


admin.site.register(Video)
